import {FETCH_PROFILES_REQUEST, FETCH_PROFILES_SUCCESS, FETCH_PROFILES_FAILURE} from './ActionTypes'


const Initial_state = {
    loading: false,
    profiles: [],
    error: ''
}

const Reducer = (state = Initial_state, action) => {
    switch(action.type){
        case FETCH_PROFILES_REQUEST:
            return{
                ...state,
                loading: true
            }
        case FETCH_PROFILES_SUCCESS:
            return{
                loading: false,
                profiles : action.payload,
                error: ''
            }
        case FETCH_PROFILES_FAILURE:
            return{
                loading: false,
                profiles: [],
                error: action.payload
            }
        default:
            return state
    }
}

export default Reducer


