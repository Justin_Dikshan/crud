import {createStore, applyMiddleware } from "redux";
import  thunk  from 'redux-thunk';
import rootReducer from './RootReducer';
import logger from 'redux-logger';
import { composeWithDevTools } from "redux-devtools-extension";

const middleware = [logger, thunk] // creates the logger and thunk

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(...middleware)));  // creates the store applying the middleware