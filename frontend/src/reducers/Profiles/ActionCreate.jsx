import {FETCH_PROFILES_REQUEST, FETCH_PROFILES_SUCCESS, FETCH_PROFILES_FAILURE} from './ActionTypes'
import axios from 'axios';

export const fetchProfileRequest = () => {
    return{
        type: FETCH_PROFILES_REQUEST
    }
}

export const fetchProfileSuccess = (profiles) => {
    return{
        type: FETCH_PROFILES_SUCCESS,
        payload : profiles
    }
}

export const fetchProfileFailure = (error) => {
    return{
        type: FETCH_PROFILES_FAILURE,
        payload: error
    }
}

export const fetchProfiles = () => {
    /// dispatch the profiles
    return (dispatch) => {
        dispatch(fetchProfileRequest())
        axios.get('http://127.0.0.1:8000/api/profiles/')
            .then(response => {
                const profiles = response.data
                console.log(profiles)
                dispatch(fetchProfileSuccess(profiles))
            })
            .then(data => {console.log(data)})
            .catch(error => {
                const errormsg = error.message
                dispatch(fetchProfileFailure(errormsg))
            })
    }
}
