import  Reducer  from './ActionReducers'
import { combineReducers } from "redux";

// combines all reducers into root reducer
const rootReducer = combineReducers({
    profile_reducer : Reducer
})

export default rootReducer
