import React from 'react';
import { Provider } from 'react-redux';
import {store} from "./reducers/Profiles/store";
import Form from "./components/Form/Form";
import Navbar from "./components/navbar/navbar";
import { Route } from 'react-router-dom'
import { BrowserRouter} from "react-router-dom";

function App() {
    return (
        <Provider store={store}>
            <div className="App">
                <BrowserRouter>
                    <Navbar/>
                    <Route exact={true} path='/' component={Form}/>
                </BrowserRouter>
            </div>
        </Provider>

    );
}

export default App;
