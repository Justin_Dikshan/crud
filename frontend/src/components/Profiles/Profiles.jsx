import React, { useEffect } from 'react';
import { useSelector } from "react-redux";
import { connect } from 'react-redux'
import { fetchProfiles } from '../../reducers/Profiles';


function Profiles({fetchProfiles, editData}){

    useEffect(() => {
        fetchProfiles()}, [])

    const profile_objects = useSelector((state)=> state.profile_reducer)

    const DeleteItem = (item) => {
        const url_pattern = `http://127.0.0.1:8000/api/profiles/${item}/`;
        fetch(url_pattern,{
            method: "DELETE",
        }).then(response => {
            window.location.href = "/";
        })
    }

    if (profile_objects.loading) {
        return <h3>loading</h3>
    } else if (profile_objects.error) {
        return <h3>error</h3>
    } else {
        return <div className="d-flex justify-content-between">
            <table className="table" style={{ width: "40%", display: 'inline-block'}}>
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Age</th>
                    <th scope="col">Home Town</th>
                    <th scope="col">Delete</th>
                    <th scope="col">Update</th>
                </tr>
                </thead>
                <tbody>
                {profile_objects.profiles.map((profile) => {
                    return <tr key={profile.id}>
                        <th scope="row">{profile.id}</th>
                        <td>{profile.first_name}</td>
                        <td>{profile.last_name}</td>
                        <td>{profile.age}</td>
                        <td>{profile.hometown}</td>
                        <td><button className="btn bg-primary" type="submit" onClick={() => DeleteItem(profile.id)}>Delete</button></td>
                        <td><button className="btn bg-success" onClick={() => editData(profile)}>Update</button></td>
                    </tr>
                })}

                </tbody>
            </table>
        </div>
    }
}

const mapDispatchToProps = dispatch => {
    return{
        fetchProfiles : () => dispatch(fetchProfiles())
    }
}

export default connect(null, mapDispatchToProps) (Profiles)