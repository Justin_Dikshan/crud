import React from 'react';
import Profiles from "../Profiles/Profiles";


class Form extends React.Component {
    constructor() {
        super();
        this.state = {
            items : {
                id: '',
                first_name : '',
                last_name : '',
                age : '',
                hometown : '',
            },
            editing: false,
        }
    }

    inputChange = e => {
        /// change the state items onchange of input values
        this.setState({
            items: {
                ...this.state.items,
                [e.target.name] : e.target.value
            }
        })
    }


    submitData = e => {
        e.preventDefault();
        /// submits the form data based on new data or edit data
        let url = 'http://127.0.0.1:8000/api/profiles/';
        if (this.state.editing === true){
            // checks if editing is true
            fetch(`http://127.0.0.1:8000/api/profiles/${this.state.items.id}/`, {
                method: 'PUT',
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(this.state.items),
            }).then(response => {
                window.location.href = "/";
            })
            this.setState({editing: false})

        } else if (this.state.editing === false){
            fetch(url, {
                method: "POST",
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(this.state.items),
            }).then((response) => {
                    console.log(response);
                    window.location.href = "/";
                }
            )
        }
    };


    editData = (profile) => {
        // gets the item and change into edit mode
        this.setState({
            items : profile,
            editing : true
        })
    }

    render() {
        return (
            <div className="border d-flex justify-content-between" >
                <form onSubmit={this.submitData} style={{width: "40%", display: 'inline-block'}}>
                    <div className="row justify-content-md-center mt-4 ">
                        <h3>Add Profile</h3>
                    </div>
                    <div className="form-group ml-4 mr-4">
                        <input type="text" className="form-control" name="first_name" value={this.state.items.first_name}
                               placeholder="First Name" onChange={this.inputChange}/>
                    </div>
                    <div className="form-group ml-4 mr-4">
                        <input type="text" className="form-control" name="last_name" placeholder="Last Name" value={this.state.items.last_name}
                               onChange={this.inputChange}/>
                    </div>
                    <div className="form-group ml-4 mr-4">
                        <input type="text" className="form-control" name="age" placeholder="Age" value={this.state.items.age} onChange={this.inputChange}/>
                    </div>
                    <div className="form-group ml-4 mr-4">
                        <input type="text" className="form-control" name="hometown" value={this.state.items.hometown} placeholder="Hometown"
                               onChange={this.inputChange}/>
                    </div>
                    <div className="row justify-content-md-center mt-4 ">
                        <button className="btn btn-success ml-4 mb-2" type="submit">Submit</button>
                    </div>
                </form>
                <Profiles editData={this.editData}/>
            </div>
        )
    }

}

export default Form