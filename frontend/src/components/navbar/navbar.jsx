import React from 'react'
import {Link} from 'react-router-dom'

function Navbar() {
    return <div id="nav_div" >
        <nav id="navigation_bar" className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="collapse navbar-collapse drop-menu show" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto" style={{ color: "white" }}>
                    <li className="nav-item">
                         <Link to='/' className="nav-link">Home</Link>
                    </li>
                </ul>

            </div>
        </nav>
    </div>
}

export default Navbar