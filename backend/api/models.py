from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=20, null=True)


class Profile(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    age = models.IntegerField()
    hometown = models.CharField(max_length=20)

    def __str__(self):
        return f'{self.first_name} {self.last_name} profile'
